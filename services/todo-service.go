package services

import (
	"todoappbackend/models"
	"todoappbackend/repositories"
)

type ToDoServiceInterface interface {
	GetAllTodos() interface{}
	AddTodo(subject string) interface{}
}

type TodoService struct {
	repository repositories.ToDoRepositoryInterface
}

func NewTodoService(repository repositories.ToDoRepositoryInterface) ToDoServiceInterface {
	return TodoService{repository}
}

func (s TodoService) GetAllTodos() interface{} {
	return s.repository.FindAll()
}

func (s TodoService) AddTodo(subject string) interface{} {
	todo := models.TodoModel{
		Subject: subject,
	}
	return s.repository.Add(todo)
}

func (s TodoService) DeleteTodo(id string) {
	s.repository.Remove(id)
}
