// Code generated by MockGen. DO NOT EDIT.
// Source: .\services\todo-service.go

// Package services is a generated GoMock package.
package services

import (
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockToDoServiceInterface is a mock of ToDoServiceInterface interface.
type MockToDoServiceInterface struct {
	ctrl     *gomock.Controller
	recorder *MockToDoServiceInterfaceMockRecorder
}

// MockToDoServiceInterfaceMockRecorder is the mock recorder for MockToDoServiceInterface.
type MockToDoServiceInterfaceMockRecorder struct {
	mock *MockToDoServiceInterface
}

// NewMockToDoServiceInterface creates a new mock instance.
func NewMockToDoServiceInterface(ctrl *gomock.Controller) *MockToDoServiceInterface {
	mock := &MockToDoServiceInterface{ctrl: ctrl}
	mock.recorder = &MockToDoServiceInterfaceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockToDoServiceInterface) EXPECT() *MockToDoServiceInterfaceMockRecorder {
	return m.recorder
}

// AddTodo mocks base method.
func (m *MockToDoServiceInterface) AddTodo(subject string) interface{} {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "AddTodo", subject)
	ret0, _ := ret[0].(interface{})
	return ret0
}

// AddTodo indicates an expected call of AddTodo.
func (mr *MockToDoServiceInterfaceMockRecorder) AddTodo(subject interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AddTodo", reflect.TypeOf((*MockToDoServiceInterface)(nil).AddTodo), subject)
}

// GetAllTodos mocks base method.
func (m *MockToDoServiceInterface) GetAllTodos() interface{} {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAllTodos")
	ret0, _ := ret[0].(interface{})
	return ret0
}

// GetAllTodos indicates an expected call of GetAllTodos.
func (mr *MockToDoServiceInterfaceMockRecorder) GetAllTodos() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAllTodos", reflect.TypeOf((*MockToDoServiceInterface)(nil).GetAllTodos))
}
