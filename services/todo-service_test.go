package services

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
	"todoappbackend/models"
	"todoappbackend/repositories"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func Test_ShouldGetAllTodos(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	var todoList []models.TodoModel
	objectId := primitive.NewObjectID()
	todoList = append(todoList, models.TodoModel{
		Id:      objectId,
		Subject: "hello world",
	})

	repository := repositories.NewMockToDoRepositoryInterface(controller)
	repository.EXPECT().FindAll().Return(todoList).Times(1)
	service := TodoService{repository}

	all := service.GetAllTodos().([]models.TodoModel)
	assert.NotNil(t, all)
	assert.NotEmpty(t, all)
	assert.Equal(t, 1, len(all))
	assert.Equal(t, objectId, all[0].Id)
	assert.Equal(t, "hello world", all[0].Subject)
}
