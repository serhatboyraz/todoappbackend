package handlers

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"todoappbackend/models"
	"todoappbackend/services"

	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

type CreateTodoRequest struct {
	Subject string `json:"subject"`
}

func Test_ShouldReturnEmptyTodoList(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	todos := make([]models.TodoModel, 0)
	service := services.NewMockToDoServiceInterface(controller)
	service.EXPECT().GetAllTodos().Return(todos).Times(1)

	handler := TodoHandler{todoService: service}
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/todos", nil)
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)

	expected := `[]
`

	if assert.NoError(t, handler.GetAllTodos(ctx)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, expected, rec.Body.String())
	}
}
