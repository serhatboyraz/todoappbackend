package handlers

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"todoappbackend/models"
	"todoappbackend/services"
)

type ToDoHandlerInterface interface {
	GetAllTodos(c echo.Context) error
	AddTodo(c echo.Context) error
}

func NewTodoHandler(todoService services.ToDoServiceInterface) ToDoHandlerInterface {
	return TodoHandler{todoService}
}

type TodoHandler struct {
	todoService services.ToDoServiceInterface
}

func (handler TodoHandler) GetAllTodos(c echo.Context) error {
	return c.JSON(http.StatusOK, handler.todoService.GetAllTodos())
}

func (handler TodoHandler) AddTodo(c echo.Context) error {
	t := new(models.TodoModel)
	if err := c.Bind(t); err != nil {
		return c.JSON(http.StatusBadRequest, "")
	}

	return c.JSON(http.StatusCreated, handler.todoService.AddTodo(t.Subject))
}
