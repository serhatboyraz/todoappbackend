package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type TodoModel struct {
	Id      primitive.ObjectID `bson:"_id" json:"id"`
	Subject string             `bson:"subject" json:"subject"`
}
