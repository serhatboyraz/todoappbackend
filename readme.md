#Todo App Backend (Golang)

## Build and Tests
### Building the project

Project setup
```
$ go install
```

Compiles
```
$ go build -o main .
```

### Running tests

Run your unit tests
```
$ go test todoappbackend/services
$ go test todoappbackend/handlers
```


##Development

Create Mocks : 
```
mockgen -source .\repositories\todo-repository.go -destination .\repositories\mock_todo-repository.go -package repositories
mockgen -source .\services\todo-service.go -destination .\services\mock_todo-service.go -package services
```

