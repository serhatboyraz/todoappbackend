package repositories

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"todoappbackend/models"
)

type ToDoRepositoryInterface interface {
	FindAll() []models.TodoModel
	Add(insertTodo models.TodoModel) models.TodoModel
	Remove(id string)
}
type ToDoRepository struct {
	collection *mongo.Collection
}

func NewToDoRepository(collection *mongo.Collection) ToDoRepository {
	return ToDoRepository{collection: collection}
}

func (t ToDoRepository) FindAll() []models.TodoModel {
	var todos = make([]models.TodoModel, 0)
	found, _ := t.collection.Find(context.Background(), bson.D{})
	_ = found.All(context.Background(), &todos)
	return todos
}

func (t ToDoRepository) Add(insertTodo models.TodoModel) models.TodoModel {
	insertTodo.Id = primitive.NewObjectID()
	id, _ := t.collection.InsertOne(context.Background(), insertTodo)
	objectId, _ := id.InsertedID.(primitive.ObjectID)
	filter := bson.M{"_id": objectId}
	one := t.collection.FindOne(context.Background(), filter)
	var todo models.TodoModel
	_ = one.Decode(&todo)
	return todo
}

func (t ToDoRepository) Remove(id string) {
	objectId, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id": objectId}
	_, _ = t.collection.DeleteOne(context.Background(), filter)

}
