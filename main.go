package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"os"
	"todoappbackend/config"
	"todoappbackend/handlers"
	"todoappbackend/repositories"
	"todoappbackend/services"
)

func main() {
	e := echo.New()
	e.Use(middleware.CORS())

	config := new(config.MongoConfiguration).Init(getDbUri(), "todoapp")
	todoRepository := repositories.NewToDoRepository(config.Database().Collection("todos"))
	todoService := services.NewTodoService(todoRepository)
	todoHandler := handlers.NewTodoHandler(todoService)

	e.GET("/todo", todoHandler.GetAllTodos)
	e.PUT("/todo", todoHandler.AddTodo)

	e.Logger.Fatal(e.Start(":1323"))
}

func getDbUri() string {
	uri := os.Getenv("MONGO_URI")
	if uri == "" {
		return "mongodb://localhost:27017"
	}
	return uri
}
